<?php

// $Id: hitasoft_player_profiles.inc,v 1.0 Created by Vasanth at 2010/01/13 11:10:29 Exp $
/*
 * This file is used to edit the video details.
 */
/* * ****************************************************** Hitasoft VIDEO FORM ************************************************************************ */
/*
 * Edit videos from elements.
 */
function hitasoft_player_video_edit_form(&$form_state,$pid=null, $id) {

    /* Add css and js. */

    drupal_add_js(drupal_get_path('module', 'hitasoft_player') . '/js/ajaxupload.3.5.js');
    drupal_add_js(drupal_get_path('module', 'hitasoft_player') . '/js/jquery-1.3.2.js');
    drupal_add_js(drupal_get_path('module', 'hitasoft_player') . '/js/upload_script.js');
    drupal_add_js(drupal_get_path('module', 'hitasoft_player') . '/js/videoadd.js');

    drupal_add_css(drupal_get_path('module', 'hitasoft_player') . '/style/hitasoft_playeradd.css');

    $form = array();

    /* Set tittle for this page. */

    drupal_set_title(t('Add Videos'));

    /* Construct the form elements for addvideos. */

    $form['#prefix'] = '<div style="clear:both;"></div>';
	
	$form['width'] = array(
        '#title' => t('Video Width'),
        '#type' => 'textfield',
        '#name' => 'width',
        '#id' => 'width',
        '#value' => '',
        '#required' => TRUE,
		'#size' => '5',
    );
	$form['height'] = array(
        '#title' => t('Video Height'),
        '#type' => 'textfield',
        '#name' => 'height',
        '#id' => 'height',
        '#value' => '',
        '#required' => TRUE,
		'#size' => '5',
    );
	$form['title'] = array(
        '#title' => t('Video Title'),
        '#type' => 'textfield',
        '#name' => 'title',
        '#id' => 'title',
        '#value' => '',
        '#required' => TRUE,
		'#size' => '50',
    );
	$form['targeturl'] = array(
        '#title' => t('Video URL'),
        '#type' => 'textfield',
        '#name' => 'targeturl',
        '#id' => 'targeturl',
        '#value' => '',
		'#size' => '50',
    );
	$form['description'] = array(
        '#title' => t('Video Description'),
        '#type' => 'textarea',
        '#name' => 'description',
        '#id' => 'description',
        '#value' => '',
	);


	$form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Submit')
    );

    return $form;
}

/* * ************************************ handles the submit of an individual profile form********************************************************** */
/*
 * Edit videos from elements submit function.
 */

function hitasoft_player_video_edit_form_submit($form, &$form_state) {

    /* $form -- list of form atributes available. */

    /* $form_state -- Contain values of above form elements. */

    /* get the post values */

    $playlistId = ($_POST['playlistid'] != '') ? $_POST['playlistid'] : '0';

    $published = ($_POST['published'] != '') ? $_POST['published'] : '0';

    /* set addslashes for title to avoid single quotes and double quotes error. */

    $title = addslashes($_POST['title']);

    $download = $_POST['download'];

    $postrollAds = ($_POST['postroll'] != '') ? $_POST['postroll'] : '0';

    $prerollAds = ($_POST['preroll'] != '') ? $_POST['preroll'] : '0';

    $postrollId = ($_POST['postrollid'] != '') ? $_POST['postrollid'] : '0';

    $prerollId = ($_POST['prerollid'] != '') ? $_POST['prerollid'] : '0';

    $targetUrl = $_POST['targeturl'];

    $description = addslashes($_POST['description']);

    $ordering = ($_POST['order'] != '') ? $_POST['order'] : '0';

    $streamerOption = $_POST['stream'];

    $id = $_POST['id'];

    if ($id != '') {

        /* Retrive the datas from hitasoft_player_videos. */

        $result = db_query("SELECT * FROM {hitasoft_player_videos} WHERE id = $id");

        /* fetch the database values using foreach */

        foreach ($result as $record) {

        }
    }

    $ffmpegVideos = '';

    $ffmpegHd = '';

    $ffmpegThumbimages = '';

    $ffmpegPreviewimages = '';

    $duration = '';

    $thumbUrl = '';

    $videoUrl = '';

    $hdUrl = '';

    /* Delete old video and thumb image. */

    /* Get file path */

    $path = file_default_scheme() . '://';

    $filePath = drupal_realpath($path);

    if ($record->videourl != '' && $record->filepath == 'upload') {

        $uploadDir = "$filePath/hitasoft_player/videos/$record->videourl";

        drupal_unlink($uploadDir);
    }
    if ($record->hdurl != '' && $record->filepath == 'upload') {

        global $base_url;

        $uploadDir = "$filePath/hitasoft_player/hdvideos/$record->hdurl";

        drupal_unlink($uploadDir);
    }
    if ($record->thumburl != '' && $record->filepath == 'upload') {

        global $base_url;

        $uploadDir = "$filePath/hitasoft_player/thumb/$record->thumburl";

        drupal_unlink($uploadDir);
    }


    /* If radiobutton value is youtube make thumnail and preview image. */

    if ($_POST['radiobutton'] == 'youtube') {

        $ytb_pattern = "@youtube.com\/watch\?v=([0-9a-zA-Z_-]*)@i";

        $filePath = 'youtube';

        if (preg_match($ytb_pattern, stripslashes($_POST['youtube-value']), $match)) {

            /* Get youtube details. */

            $youtube_data = hd_GetSingleYoutubeVideo($match[1]);

            if ($youtube_data) {

                $thumbnailUrl = $youtube_data['thumbnail_url'];

                $thumbUrl = $thumbnailUrl;
            }
            $youtubeValue = $_POST['youtube-value'];

            $videoUrl = $youtubeValue;

            $hdUrl = '';
        } else {
            drupal_set_message(t('Please enter a A Valid YouTube URL!'));
        }
    }

    /* If radiobutton value is url. */

    if ($_POST['radiobutton'] == 'url') {

        if ($_POST['customurl'] == '') {
            drupal_set_message(t('Enter A Valid URL!'));
        } else {

            $videoPath = $_POST['customurl'];

            if ($videoPath != '') {
                $videoUrl = $videoPath;
            }

            $hdPath = $_POST['customhd'];

            if ($hdPath != '') {
                $hdUrl = $hdPath;
            }

            $thumbPath = $_POST['customimage'];

            if ($thumbPath != '') {

                $previewPath = $_POST['custompreimage'];

                if ($previewPath != '') {

                    $previewUrl = $previewPath;
                }

                $thumbUrl = $thumbPath;
            }

            $filePath = 'url';
        }
    }
    if ($_POST['radiobutton'] == 'ffmpeg') {

        $ffmpegVideos = $_POST['ffmpeg_videos'];

        $ffmpegThumbimages = $_POST['ffmpeg_thumbimages'];

        $ffmpegPreviewimages = $_POST['ffmpeg_previewimages'];

        $ffmpegHd = $_POST['ffmpeg_hd'];

        $video = url() . '/sites/default/files/hitasoft_player/hdvideos/';

        $fileName = $_POST['ffmpegform-value'];

        $ffmpeg_path = variable_get('hitasoft_player_ffmpeg', '').'ffmpeg.exe';

        $file = explode('.', $fileName);

        $imgPath = variable_get('hitasoft_player_ffmpeg', '');

        $destFile = $video . $fileName;

        $jpgResolution = "320x240";

        $targetPath = $file[0] . ".jpeg";

        /*
         * Convert any type of videos in to flv and get thumb image from frames of video.
         * Using ffmpeg.
         */

        exec($ffmpeg_path . ' -i ' . $destFile . ' -ab 56 -ar 44100 -b 200 -r 15 -s 320x240 -f flv ' . $imgPath . '\finale.flv');

        exec($ffmpeg_path . ' -i ' . $destFile . ' -an -ss 3 -t 00:00:01 -r 1 -y -vcodec mjpeg -f mjpeg ' . $imgPath . '\thumbimage.jpg');

        $ffmpegVideos = $file[0] . ".flv";

        $ffmpegHd = $_POST['ffmpegform-value'];

        $ffmpegThumbimages = $file[0] . ".jpg";

        $ffmpegPreviewimages = $ffmpegThumbimages;

        $filePath = 'ffmpeg';
    }

    /* If radiobutton value is upload. */

    if ($_POST['radiobutton'] == 'upload') {

        $videoPath = $_POST['normalvideoform-value'];

        if ($videoPath != '') {
            $videoUrl = $videoPath;
        }

        $hdPath = $_POST['hdvideoform-value'];

        if ($hdPath != '') {
            $hdUrl = $hdPath;
        }

        $thumbPath = $_POST['thumbimageform-value'];

        if ($thumbPath != '') {
            $thumbUrl = $thumbPath;
        }

        $filePath = 'upload';
    }

    /* If the file path is  Rtmp */

    if ($_POST['stream'] == 'rtmp') {

        $streamerPath = $_POST['streamerpath'];

        $isLive = $_POST['islive'];
    } else {

        $streamerPath = '';

        $isLive = 0;
    }

    /* database query for update the values into database. */

    db_update('hitasoft_player_videos')
            ->fields(array('published' => $published, 'title' => $title, 'ffmpeg_videos' => $ffmpegVideos,
                'ffmpeg_thumbimages' => $ffmpegThumbimages, 'ffmpeg_previewimages' => $ffmpegPreviewimages, 'ffmpeg_hd' => $ffmpegHd,
                'filepath' => $filePath, 'videourl' => $videoUrl, 'thumburl' => $thumbUrl, 'previewurl' => $thumbUrl, 'hdurl' => $hdUrl, 'playlistid' => $playlistId, 'duration' => 0,
                'ordering' => $ordering, 'streamerpath' => $streamerPath, 'streameroption' => $streamerOption, 'postrollads' => $postrollAds, 'prerollads' => $prerollAds,
                'description' => $description, 'targeturl' => $targetUrl, 'download' => $download, 'prerollid' => $prerollId, 'postrollid' => $postrollId,
                'islive' => $isLive))
            ->condition('id', $id)
            ->execute();

    /* Set message after insert the values into database. */

    drupal_set_message(t('Updated into Database'));

    /* Set Redirect path after insert the values into database. */

    $form_state['redirect'] = 'admin/hitasoft_player/videos/list';
}

/* * ********************************************************** Return Youtube single video******************************************************** */

function hd_GetSingleYoutubeVideo($youtube_media) {

    if ($youtube_media == '')
        return;
 
    $url = 'http://gdata.youtube.com/feeds/api/videos/' . $youtube_media;

    $ytb = hd_ParseYoutubeDetails(hd_GetYoutubePage($url));

    return $ytb[0];
}

/* * ********************************************************* Parse xml from Youtube****************************************************************** */

function hd_ParseYoutubeDetails($ytVideoXML) {

    // Create parser, fill it with xml then delete it
    $yt_xml_parser = xml_parser_create();

    xml_parse_into_struct($yt_xml_parser, $ytVideoXML, $yt_vals);

    xml_parser_free($yt_xml_parser);
    // Init individual entry array and list array
    $yt_video = array();

    $yt_vidlist = array();

    // is_entry tests if an entry is processing
    $is_entry = true;
    // is_author tests if an author tag is processing
    $is_author = false;
    foreach ($yt_vals as $yt_elem) {

        // If no entry is being processed and tag is not start of entry, skip tag
        if (!$is_entry && $yt_elem['tag'] != 'ENTRY')
            continue;

        // Processed tag
        switch ($yt_elem['tag']) {
            case 'ENTRY' :
                if ($yt_elem['type'] == 'open') {

                    $is_entry = true;

                    $yt_video = array();
                } else {

                    $yt_vidlist[] = $yt_video;

                    $is_entry = false;
                }
                break;
            case 'ID' :

                $yt_video['id'] = substr($yt_elem['value'], -11);

                $yt_video['link'] = $yt_elem['value'];

                break;

            case 'PUBLISHED' :

                $yt_video['published'] = substr($yt_elem['value'], 0, 10) . ' ' . substr($yt_elem['value'], 11, 8);

                break;
            case 'UPDATED' :

                $yt_video['updated'] = substr($yt_elem['value'], 0, 10) . ' ' . substr($yt_elem['value'], 11, 8);

                break;
            case 'MEDIA:TITLE' :

                $yt_video['title'] = $yt_elem['value'];

                break;
            case 'MEDIA:KEYWORDS' :

                $yt_video['tags'] = $yt_elem['value'];

                break;
            case 'MEDIA:DESCRIPTION' :

                $yt_video['description'] = $yt_elem['value'];

                break;
            case 'MEDIA:CATEGORY' :

                $yt_video['category'] = $yt_elem['value'];

                break;
            case 'YT:DURATION' :

                $yt_video['duration'] = $yt_elem['attributes'];

                break;
            case 'MEDIA:THUMBNAIL' :
                if ($yt_elem['attributes']['HEIGHT'] == 240) {

                    $yt_video['thumbnail'] = $yt_elem['attributes'];

                    $yt_video['thumbnail_url'] = $yt_elem['attributes']['URL'];
                }
                break;
            case 'YT:STATISTICS' :

                $yt_video['viewed'] = $yt_elem['attributes']['VIEWCOUNT'];

                break;
            case 'GD:RATING' :

                $yt_video['rating'] = $yt_elem['attributes'];

                break;
            case 'AUTHOR' :

                $is_author = ($yt_elem['type'] == 'open');

                break;
            case 'NAME' :
                if ($is_author)
                    $yt_video['author_name'] = $yt_elem['value'];

                break;
            case 'URI' :
                if ($is_author)
                    $yt_video['author_uri'] = $yt_elem['value'];

                break;
            default :
        }
    }

    unset($yt_vals);

    return $yt_vidlist;
}

/* * ************************************************************ Returns content of a remote page************************************************************ */
/* Still need to do it without curl */

function hd_GetYoutubePage($url) {

    // Try to use curl first
    if (function_exists('curl_init')) {

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $xml = curl_exec($ch);

        curl_close($ch);
    }
    // If not found, try to use file_get_contents (requires php > 4.3.0 and allow_url_fopen)
    else {
        $xml = @file_get_contents($url);
    }

    return $xml;
}