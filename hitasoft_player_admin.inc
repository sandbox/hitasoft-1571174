<?php

// $Id: hitasoft_player.admin.inc,v 1.0 Created by Vasanth at 2010/01/12 20:22:57 goba Exp $
/*
 * This file is contain form elements of Player configuration, language setting, page setting, google adsense.
 * And these details are stored in table variable in drupal.
 */
/* * ************************************************************** Manage video function Route Path ************************************************ */
/*
 * Videos List page.
 */
function hitasoft_player_admin() {}

/* * ************************************************************** PLAYER SETTINGS FORM **************************************************************** */

function hitasoft_player_admin_settings($form_id, &$form_state) {
	$form['#prefix'] = '<div style="clear:both;"><table align="center" cellpadding="5">
<tr><td>1.&nbsp;&nbsp; Download the module zip file.</td></tr>
<tr><td>2.&nbsp;&nbsp;  Unzip the module zip file and extract all the files.</td></tr>
<tr><td>3.&nbsp;&nbsp; Create a new folder under modules and name it "ripe_player". Now the module path should $root/module/ripe_player.<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Copy all the extracted module files and paste it to ripe_player folder. Make sure you are moving the files directly into the folder. So the module files path will be like $root/module/ripe_player/hitasoft_player.install and the same for other necessary files also.<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Now login to admin panel and select the "Ripe HD Player" menu which will show the documentation and manage video links.</td></tr>
<tr><td>4.&nbsp;&nbsp; There will be a default video in the module, if you want to add more video, click "Ripe HD Player>Manage Videos>Add Video".</td></tr>
<tr><td>5.&nbsp;&nbsp; The default video url will be "www.yourdomain.com/hitasoft_player-gallery/[Your Video ID]".</td></tr>
<tr><td>6.&nbsp;&nbsp; If you want add more video then click Add Video</td></tr>
<tr><td>7. You video show Navigation Tab inside HD Video Gallery or This would be the path: www.yourdomain.com/hitasoft_player-gallery/[YOUR VIDEO ID</td></tr>
<tr><td>8.&nbsp;&nbsp; If you click the "HD Video Gallery" you will be redirected your latest/recently added video page.</td></tr>
</table></div>';
    return $form;
}

/* * ************************************************************** LANGUAGE SETTINGS FORM **************************************************************** */

function hitasoft_player_admin_language(&$form_state) {}

