<?php

// $Id: hitasoft_player_profiles.inc,v 1.0 Created by Vasanth at 2010/01/19 12:19:29 Exp $

/* * ****************************************************** Hitasoft Videos Details in list ************************************************************************ */
/*
 * Retrive playlist details from hitasoft_player_videos.
 */

function hitasoft_player_listvideos_form() {


    $result = db_query('SELECT * FROM {hitasoft_player_videos}');

    $form = array();

    $tables = array();

    $output = '';

    foreach ($result as $tables) {
		$output .= '<tr><td>' . $tables->id . '</td><td>' . $tables->title . '</td><td>' . $tables->video . '</td><td>' . $tables->width . '</td><td>' . $tables->height . '</td><td><a href="delete/' . $tables->id . '">Delete</a></td></tr>';
    }

    /* Header topics. */

    $form['#prefix'] = '<table class="widefat" cellspacing="0">
           <table class="widefat" cellspacing="0" style="width:800px;">
            <thead>
                <tr>
                    <th id="id" scope="col">ID </th>
                    <th id="title" scope="col">Video Title </th>                   
                     <th id="path"  scope="col">Video Url</th>
                    <th id="path"   scope="col">Video Width</th>
					<th id="path"   scope="col">Video Height</th>
                    <th id="path"   scope="col"><a href="addvideo/">Add Video </a></th>
                </tr>
            </thead><tbody>' . $output . '</tbody></table>';

    return $form;
}
