<?php
	 
	 $arrMainStr = explode(';',$_REQUEST['mainurl']);
	 $baseUrl = $arrMainStr[0];
	 $videoWidth = str_replace('video_width=','',$arrMainStr[1]);
	 $videoHeight = str_replace('video_height=','',$arrMainStr[2]);
	 $videoUrl = str_replace('video_url=','',$arrMainStr[3]);
	 $videoTitle = str_replace('video_title=','',$arrMainStr[4]);
	 $videoDesc = str_replace('video_desc=','',$arrMainStr[5]); 
     $skinPath = $baseUrl . '/hitasoft_player/skin/hdskin.swf';
	 $hdPlayerPath = $baseUrl . '/hitasoft_player/HDPlayer.swf';
     $languageXML = $baseUrl . '/hitasoft_player/xml/lang/en-EN.xml';
    
	/* Create config xml file for hdflv player */
	$modulePathSwf = $baseUrl . '/hitasoft_player/HDPlayer.swf';
	$br      = "\n";
	ob_clean();
	header("content-type:text/xml;charset=utf-8");

	$html  = '<?xml version="1.0" encoding="utf-8"?>'.$br;
	$html .= '<HDPlayer>'.$br;
	$html .= '<Skins>'.$br;
	$html .= '<Skin  Name="SkinFile" Path="'.$skinPath.'"/>'.$br;
	$html .= '</Skins>'.$br;
	$html .= '<DisplaySettings>'.$br;
	$html .= '<Controls>'.$br;
	$html .= '<Control Name="playerControls" Visible="true" AutoHide="true" JSApiEnabled="true"/>'.$br;
	$html .= '<Control Name="sidebarControls" Visible="true" AutoHide="true">'.$br;
	$html .= '<ControlItem Name="emailFriend" Enabled="true" />'.$br;
	$html .= '<ControlItem Name="videoSize" Enabled="true" />'.$br;
	$html .= '<ControlItem Name="embedVideo" Enabled="true" />'.$br;
	$html .= '<ControlItem Name="videoTitle" Enabled="true"/>'.$br;
	$html .= '<ControlItem Name="hdOnOff" Enabled="true" />'.$br;
	$html .= '<ControlItem Name="ToolTip" Enabled="true" />'.$br;
	$html .= '</Control>'.$br;	
	$html .= '</Controls>'.$br;
	$html .= '<Logo FilePath="logo.swf" URL="http://www.hitasoft.com"  Target="_blank" Opacity="50"/>'.$br;
	$html .= '<Language XmlPath="'.$languageXML.'" default=""/>'.$br;
	$html .= '</DisplaySettings>'.$br;
	$html .= '<AdvertiseMents>'.$br;
	$html .= '<PreRoll Type="image" DurationInSeconds="5" Enabled="true">'.$br;	
	$html .= '</PreRoll>'.$br;
	$html .= '<MidRoll  Enabled="true" Duration="5">'.$br;
	$html .= '</MidRoll>'.$br;
	$html .= '<PostRoll Type="image" DurationInSeconds="5" Enabled="true">'.$br;	
	$html .= '</PostRoll>'.$br;
	$html .= '</AdvertiseMents>'.$br;
	$html .= '<ButtonControls>'.$br;
	$html .= '<EmbedCode><![CDATA[<embed src="'.$hdPlayerPath.'" quality="high" bgcolor="#000000" width="'.$videoWidth.'" height="'.$videoHeight.'" name="HDPlayer" align="middle" allowScriptAccess="sameDomain" allowFullScreen="true" type="application/x-shockwave-flash" pluginspage="http://www.adobe.com/go/getflashplayer" />]]></EmbedCode>'.$br;
	$html .= '<LinkCode><![CDATA[http://www.yourdomain.com/videofile.flv]]></LinkCode>'.$br;
	$html .= '<Share EmailPHPPath="email.php"/>'.$br;
	$html .= '</ButtonControls>'.$br;
	$html .= '<VideoPlayBack DefaultVolume="10" HardwareFullScreenScaling="false"  StreamingType="http" RtmpServer="">'.$br;
	$html .= '<Video>'.$br;
	$html .= '<File HDVideoPath="'.$videoUrl.'" AutoPlay="false" VideoTitle="'.$videoTitle.'" VideoDescription="'.$videoDesc.'" TitleClickUrl="http://www.hitasoft.com/newhome/" VideoSource="" VideoQuality=""/>'.$br;
	
	$html .= '</Video>'.$br;
	$html .= '</VideoPlayBack>'.$br;
	$html .= '<RelatedVideoList XmlPath="http://www.hd-player.net/files/hdplayer/xml/videolist.xml" Enabled="true"/>'.$br;
	$html .= '<SocialNetworkShare>'.$br;
	$html .= '<FaceBook APIUrl="http://www.facebook.com/sharer.php?" ShareContent=""/>'.$br;
	$html .= '<Twitter APIUrl="http://twitter.com/share?url=" ShareContent=""/>'.$br;
	$html .= '<Delicious APIUrl="http://delicious.com/post?url=" ShareContent="www.hitasoft.com/newhome"/>'.$br;
	$html .= '</SocialNetworkShare>'.$br;
	$html .= '</HDPlayer>'.$br;
	echo $html;
	exit();

?>