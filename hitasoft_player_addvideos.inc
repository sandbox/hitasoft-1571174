<?php

// $Id: hitasoft_player_addvideos.inc,v 1.0 Created by Vasanth at 2010/01/19 11:7:12 Exp $
/*
 * This file is used to add the videos. in the the form of file upload, direct url, youtube url, custom url etc.
 */
/* * ****************************************************** Hitasoft Videos Add Form ************************************************************************ */
/*
 * Add videos from elements.
 */
function hitasoft_player_addvideos_form() {

    /* Add css and js. */

    drupal_add_js(drupal_get_path('module', 'hitasoft_player') . '/js/jquery-1.3.2.js');
    drupal_add_js(drupal_get_path('module', 'hitasoft_player') . '/js/upload_script.js');
    
	$form = array();

    /* Set tittle for this page. */

    drupal_set_title(t('Add Videos'));

    /* Construct the form elements for addvideos. */

   $form['#prefix'] = '<div style="clear:both;"><table cellpadding="0" cellspacing="15">
      <tr>
        <td><input type="checkbox" id="playerControls" name="playerControls" value="" onclick="playerSetting(this)"></td>
        <td>Player Control</td>
        <td><input type="checkbox" id="sidebarControls" name="sidebarControls" value="" onclick="playerSetting(this)"></td>
        <td>Sidebar Controls</td>
        <td><input type="checkbox" id="emailFriend" name="emailFriend" value="" onclick="playerSetting(this)"></td>
        <td>Email Friend</td>
        <td><input type="checkbox" id="videoSize" name="videoSize" value="" onclick="playerSetting(this)"></td>
        <td>Video Size</td>
      </tr>
      <tr>
        <td><input type="checkbox" id="embedVideo" name="embedVideo" value="" onclick="playerSetting(this)"></td>
        <td>Embed Video</td>
        <td><input type="checkbox" id="videoTitle" name="videoTitle" value="" onclick="playerSetting(this)"></td>
        <td>Video Title</td>
        <td><input type="checkbox" id="hdOnOff" name="hdOnOff" value="" onclick="playerSetting(this)"></td>
        <td>HD On Off</td>
        <td><input type="checkbox" id="ToolTip" name="ToolTip" value="" onclick="playerSetting(this)"></td>
        <td>Tool Tip</td>
      </tr>
    </table></div>';
	
	$form['hiddenfile'] = array(
        '#prefix' => '<input type="hidden" name="hid_playerControls" id="hid_playerControls" value="sample"  />
                     <input type="hidden" name="hid_sidebarControls" id="hid_sidebarControls" value="" />
                     <input type="hidden" name="hid_emailFriend" id="hid_emailFriend" value="" />
                     <input type="hidden" name="hid_videoSize" id="hid_videoSize" value="" />
                     <input type="hidden" name="hid_embedVideo" id="hid_embedVideo" value="" />
                    <input type="hidden" name="hid_videoTitle" id="hid_videoTitle"  value="" />
                    <input type="hidden" name="hid_hdOnOff" id="hid_hdOnOff"  value="" />
                    <input type="hidden" name="hid_ToolTip" id="hid_ToolTip"  value="" />,
        '#type' => 'hidden',
        
    );
	
	$form['width'] = array(
        '#title' => t('Video Width'),
        '#type' => 'textfield',
        '#name' => 'width',
        '#id' => 'width',
        '#value' => '',
        '#required' => TRUE,
		'#size' => '5',
    );
	$form['height'] = array(
        '#title' => t('Video Height'),
        '#type' => 'textfield',
        '#name' => 'height',
        '#id' => 'height',
        '#value' => '',
        '#required' => TRUE,
		'#size' => '5',
    );
	$form['title'] = array(
        '#title' => t('Video Title'),
        '#type' => 'textfield',
        '#name' => 'title',
        '#id' => 'title',
        '#value' => '',
        '#required' => TRUE,
		'#size' => '50',
    );
	$form['targeturl'] = array(
        '#title' => t('Video URL'),
        '#type' => 'textfield',
        '#name' => 'targeturl',
        '#id' => 'targeturl',
        '#value' => '',
		'#size' => '50',
    );
	$form['description'] = array(
        '#title' => t('Video Description'),
        '#type' => 'textarea',
        '#name' => 'description',
        '#id' => 'description',
        '#value' => '',
	);


	$form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Submit')
    );

    return $form;
}

/* * ***************************************** Hitasoft Videos Add Form submit *********************************************************************************** */
/*
 * Add videos from elements submit function.
 */

function hitasoft_player_addvideos_form_submit($form, &$form_state) {
	
	$varPlayerWidth = ($_POST['width'] != '') ? $_POST['width'] : '500';
	$varPlayerHeight = ($_POST['height'] != '') ? $_POST['height'] : '300';
	$varVideoTitle = ($_POST['title'] != '') ? $_POST['title'] : '';
	$varVideoUrl = ($_POST['targeturl'] != '') ? $_POST['targeturl'] : '';
	$varVideoDesc = ($_POST['description'] != '') ? $_POST['description'] : '';
	$varPlayerCont = ($_POST['hid_playerControls'] > 0) ? $_POST['hid_playerControls'] : 0;
	$varSidebarCont = ($_POST['hid_sidebarControls'] > 0) ? $_POST['hid_sidebarControls'] : 0;
	$varEmailFriend = ($_POST['hid_emailFriend'] > 0) ? $_POST['hid_emailFriend'] : 0;
	$varVideoSize = ($_POST['hid_videoSize'] > 0) ? $_POST['hid_videoSize'] : 0;
	$varEmbedVideo = ($_POST['hid_embedVideo'] > 0) ? $_POST['hid_embedVideo'] : 0;
	$varVideoTitle = ($_POST['hid_videoTitle'] > 0) ? $_POST['hid_videoTitle'] : 0;
	$varHdOnOff = ($_POST['hid_hdOnOff'] > 0) ? $_POST['hid_hdOnOff'] : 0;
	$varToolTip = ($_POST['hid_ToolTip'] > 0) ? $_POST['hid_ToolTip'] : 0;

    if ($_POST['targeturl'] != '') {
		$id = db_insert('hitasoft_player_videos')->fields(array('title' => $varVideoTitle, 'video' => $varVideoUrl, 'description' => $varVideoDesc, 'width' => $varPlayerWidth, 'height' => $varPlayerHeight, 'playerControls' => $varPlayerCont, 'sidebarControls' => $varSidebarCont, 'emailFriend' => $varEmailFriend, 'videoSize' => $varVideoSize, 'embedVideo' => $varEmbedVideo, 'videoTitle' => $varVideoTitle, 'hdOnOff' => $varHdOnOff, 'ToolTip' => $varToolTip))->execute();
		 
        drupal_set_message(t('Inserted into Database'));
		$form_state['redirect'] = 'admin/hitasoft_player/videos/list';
    } else {
	    drupal_set_message(t('Please enter the video url!'));
    }

}

/* * ********************************************************                         **************************************************************** */

/* Validation for play list.in add form */

function hitasoft_player_addvideos_form_validate($form, &$form_state) {
	if ($_POST['targeturl'] == '') {
        form_set_error('', t('Please enter the video url!'));
    }
}

/* * ********************************************************** Return Youtube single video******************************************************** */

function hd_GetSingleYoutubeVideo($youtube_media) {

    if ($youtube_media == '')
        return;

    $url = 'http://gdata.youtube.com/feeds/api/videos/' . $youtube_media;

    $ytb = hd_ParseYoutubeDetails(hd_GetYoutubePage($url));

    return $ytb[0];
}

/* * ********************************************************* Parse xml from Youtube****************************************************************** */

function hd_ParseYoutubeDetails($ytVideoXML) {

    // Create parser, fill it with xml then delete it

    $yt_xml_parser = xml_parser_create();

    xml_parse_into_struct($yt_xml_parser, $ytVideoXML, $yt_vals);

    xml_parser_free($yt_xml_parser);
    // Init individual entry array and list array

    $yt_video = array();

    $yt_vidlist = array();

    // is_entry tests if an entry is processing

    $is_entry = true;

// is_author tests if an author tag is processing

    $is_author = false;

    foreach ($yt_vals as $yt_elem) {

        // If no entry is being processed and tag is not start of entry, skip tag

        if (!$is_entry && $yt_elem['tag'] != 'ENTRY')
            continue;

        // Processed tag

        switch ($yt_elem['tag']) {
            case 'ENTRY' :
                if ($yt_elem['type'] == 'open') {

                    $is_entry = true;

                    $yt_video = array();
                } else {

                    $yt_vidlist[] = $yt_video;

                    $is_entry = false;
                }
                break;
            case 'ID' :

                $yt_video['id'] = substr($yt_elem['value'], -11);

                $yt_video['link'] = $yt_elem['value'];

                break;

            case 'PUBLISHED' :

                $yt_video['published'] = substr($yt_elem['value'], 0, 10) . ' ' . substr($yt_elem['value'], 11, 8);

                break;
            case 'UPDATED' :

                $yt_video['updated'] = substr($yt_elem['value'], 0, 10) . ' ' . substr($yt_elem['value'], 11, 8);

                break;
            case 'MEDIA:TITLE' :

                $yt_video['title'] = $yt_elem['value'];

                break;
            case 'MEDIA:KEYWORDS' :

                $yt_video['tags'] = $yt_elem['value'];

                break;
            case 'MEDIA:DESCRIPTION' :

                $yt_video['description'] = $yt_elem['value'];

                break;
            case 'MEDIA:CATEGORY' :

                $yt_video['category'] = $yt_elem['value'];

                break;
            case 'YT:DURATION' :

                $yt_video['duration'] = $yt_elem['attributes'];

                break;
            case 'MEDIA:THUMBNAIL' :
                if ($yt_elem['attributes']['HEIGHT'] == 240) {

                    $yt_video['thumbnail'] = $yt_elem['attributes'];

                    $yt_video['thumbnail_url'] = $yt_elem['attributes']['URL'];
                }
                break;
            case 'YT:STATISTICS' :

                $yt_video['viewed'] = $yt_elem['attributes']['VIEWCOUNT'];

                break;
            case 'GD:RATING' :

                $yt_video['rating'] = $yt_elem['attributes'];

                break;
            case 'AUTHOR' :

                $is_author = ($yt_elem['type'] == 'open');

                break;
            case 'NAME' :
                if ($is_author)
                    $yt_video['author_name'] = $yt_elem['value'];

                break;
            case 'URI' :
                if ($is_author)
                    $yt_video['author_uri'] = $yt_elem['value'];

                break;
            default :
        }
    }

    unset($yt_vals);

    return $yt_vidlist;
}

/* * ************************************************************ Returns content of a remote page************************************************************ */
/* Still need to do it without curl */

function hd_GetYoutubePage($url) {

    // Try to use curl first
    if (function_exists('curl_init')) {

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $xml = curl_exec($ch);

        curl_close($ch);
    }
    // If not found, try to use file_get_contents (requires php > 4.3.0 and allow_url_fopen)
    else {
        $xml = @file_get_contents($url);
    }

    return $xml;
}